from pyroute2 import IPRoute
from pyroute2.netlink.rtnl import ndmsg
from pyroute2.netlink.exceptions import NetlinkError
from subprocess import call
import random
import time

# Create global obj
ip = IPRoute()
ip_list = []

# Lookup index of interface
idx = ip.link_lookup(ifname='wlp0s29f7u5')

def rand_ip_as_str():
    return str(random.randrange(0,256)) + "." + str(random.randrange(0,256)) + "." + str(random.randrange(0,256)) + "." + str(random.randrange(0,256))

def add_n_entries(n):
    while (n>0):
        ip_addr = rand_ip_as_str()
        try:
            # Add new entry with ip_addr to ARP table
            # Note: 'add' includes the NLM_F_EXCL flag
            # See https://github.com/svinota/pyroute2/blob/f4f9b1e0ef1b40cfbac57e271dd11d1fd387d598/pyroute2/iproute/linux.py (line 777)
            ip.neigh('add',
                dst=ip_addr,
                lladdr='00:11:22:33:44:55',
                ifindex=idx,
                state=ndmsg.states['permanent'])
        except NetlinkError as err:
            # If entry already exists, don't count as added
            if (err.code == 17):
                continue
            # Panic if there is another type of error
            else:
                panic("I DON'T KNOW WHAT'S HAPPENING!")

        # Record the IP address added
        ip_list.append(ip_addr)
        n -= 1

def delete_entries():
    for ip_addr in ip_list:
        try:
            # Delete new entry with ip_addr to ARP table
            ip.neigh('del',
                dst=ip_addr,
                ifindex=idx)
        except NetlinkError as err:
            panic("I DON'T KNOW WHAT'S HAPPENING!")
        
def main():
    # Expand the size of the ARP table
    call(['sysctl', '-w', 'net.ipv4.neigh.default.gc_thresh3=600000'])

    # Open the results.txt file
    results = open("results.txt", "w")
    results.write("arp_entries, query_time\n")

    num_entries = 0
    while (num_entries < 250000):
        n_to_add = 0
        if (num_entries < 10000):
            n_to_add = 1000
        elif (num_entries < 100000):
            n_to_add = 10000
        elif (num_entries < 250000):
            n_to_add = 25000

        print("About to add entries to kernel")
        print("current:", num_entries, "to_add:", n_to_add)
        add_n_entries(n_to_add)
        num_entries += n_to_add

        # Test lookup speed
        lookup_counter = 0
        while (lookup_counter < 10):
            # Choose one of the inserted IP addresses so we know it is in the ARP table
            ip_addr = random.choice(ip_list)
            start_time = time.time()
            try:
                ip.neigh('add',
                    dst=ip_addr,
                    lladdr='00:11:22:33:44:55',
                    ifindex=idx,
                    state=ndmsg.states['permanent'])
            except NetlinkError as err:
                end_time = time.time()
                if (err.code != 17):
                    print("Unknown failure")
                print(err)
            time_spent = end_time - start_time
            print(num_entries, time_spent)
            results.write(str(num_entries) + " " + str(time_spent) + '\n')

            lookup_counter += 1
    
    delete_entries()
