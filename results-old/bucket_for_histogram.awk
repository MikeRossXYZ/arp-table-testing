/ARP_TEST/ {
    if ($9 == "add") {
        bits = $11
        bucket = $13
        # If the count has not been established yet
        if (!(bits in count) || !(bucket in count[bits])) {
            count[bits][bucket] = 0
        }
        count[bits][bucket] += 1
    }
    else if ($9 == "rehash") {
        bits = $11
        bucket = $13
        # If the count has not been established yet
        if (!(bits in count) || !(bucket in count[bits])) {
            count[bits][bucket] = 0
        }
        count[bits][bucket] += 1
    }
    else if ($9 == "table_increase") {
        bits = $11
        delete count[bits]
    }
}
END {
    printf("bits,bucket,frequency\n")
    for (bits in count) {
        # Care only about large table behaviour
        if (int(bits) <= 8) {
            continue
        }
        for (j in count[bits]) {
            # Break into 20 buckets
            bucket_idx = int(j / 2**bits * 20)
            if (!(bucket_idx in buckets)) {
                buckets[bucket_idx] = 0
            }
            buckets[bucket_idx] += count[bits][j]
        }
        for (idx in buckets) {
            printf("%d,%d,%d\n", bits, idx, buckets[idx])
        }
        delete buckets
    }
}
