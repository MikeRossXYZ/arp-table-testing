BEGIN {
    printf("arp_entries,lookup_speed\n")
}
NR != 0 {
    num = substr($1,1,length($1)-1)
    if (num in speed) {
        speed[num] += $2
        count[num] += 1
    } else {
        speed[num] = $2
        count[num] = 1
    }
}
END {
    for (num in speed) {
        printf("%d,%d\n",num,speed[num]/count[num])
    }
}
