extern crate time;
extern crate eui48;
extern crate rand;

use std::fs::File;
use std::io::Write;
use std::process::Command;
use rand::{thread_rng, Rng};
use std::net::Ipv4Addr;
use eui48::MacAddress;

struct ARPEntry {
    addr: Ipv4Addr,
    /* lladdr: MacAddress, */
}

fn create_entries(n_entries: u64) -> Vec<ARPEntry> {
    let mut table: Vec<ARPEntry> = Vec::new();
    let mut rng = thread_rng();
    for _i in 0..n_entries {
        table.push(ARPEntry {
            addr: Ipv4Addr::new(rng.gen_range(0,255), rng.gen_range(0,255),
                rng.gen_range(0,255), rng.gen_range(0,255)),
            /* lladdr: MacAddress::nil(), */
        });
    }
    table
}

/*
fn print_entries(table: &Vec<ARPEntry>) {
    for i in 0..table.len() {
        println!("{}", table[i].addr.to_string());
    }
}
*/

fn add_entries_to_kernel(table: &Vec<ARPEntry>) {
    for entry_idx in 0..table.len() {
        let status = Command::new("ip")
            .arg("neigh")
            .arg("add")
            .arg(table[entry_idx].addr.to_string())
            .arg("lladdr")
            .arg(MacAddress::nil().to_hex_string())
            .arg("dev")
            .arg("enp0s3") /* depends on the VM */
            .status()
            .expect("failed to execute");
	if !status.success() {
		println!("Failed to add #{} -- {}", entry_idx, status);
	}
    }
}

fn delete_entries_to_kernel(table: &Vec<ARPEntry>) {
    for entry_idx in 0..table.len() {
        let status = Command::new("ip")
            .arg("neigh")
            .arg("delete")
            .arg(table[entry_idx].addr.to_string())
            .arg("dev")
            .arg("enp0s3") /* depends on the VM */
            .status()
            .expect("failed to execute");
	if !status.success() {
		println!("Failed to delete #{} -- {}", entry_idx, status);
	}
    }
}

fn main() {
    let mut table: Vec<ARPEntry> = Vec::new();
    let mut num_entries: u64 = 0;
    let mut rng = thread_rng();

    /* Update the max table size */
    Command::new("sysctl")
        .arg("-w")
        .arg("net.ipv4.neigh.default.gc_thresh3=600000")
        .spawn()
        .expect("Failed to increase table size!");

    /* Open output results file */
    let mut file = File::create("results.txt").expect("Unable to create file");
    file.write_all("arp_entries, query_time\n".as_bytes()).expect("Unable to write to file");

    while num_entries < 250000 {
        let n_to_add: u64 = 
            if num_entries < 1000 {
                100
            } else if num_entries < 10000 {
                1000
            } else if num_entries < 100000 {
                10000
            } else {
                25000
            };
        let mut new_entries: Vec<ARPEntry> = create_entries(n_to_add);
        println!("About to add entries to kernel");
	    println!("current: {}, to_add: {}", num_entries, n_to_add);
        add_entries_to_kernel(&new_entries);
        table.append(&mut new_entries);
	    num_entries += n_to_add;
        
        /* Test the speed of lookup, 3 rounds per ARP table size */
        for _i in 0..7 {
            let start: u64 = time::precise_time_ns();
            Command::new("ip")
                .arg("neigh")
                .arg("show")
                .arg("to")
                .arg(table[rng.gen_range(0,table.len())].addr.to_string())
                .arg("dev")
                .arg("enp0s3") /* depends on the VM */
                .status()
                .expect("failed!");
            let time_spent: u64 = (time::precise_time_ns() - start) / (1000 as u64);
            println!("{}, {}", num_entries, time_spent);
            file.write(format!("{}, {}\n", num_entries, time_spent).as_bytes())
                .expect("Unable to write to file");
        } 
    }

    /* Clean up and remove all the kernel ARP entries we made */
    delete_entries_to_kernel(&table);
}
